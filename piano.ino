#include "pitches.h"

const int btn1Pin = 5;
const int btn2Pin = 4;
const int btn3Pin = 3;
const int btn4Pin = 2;
const int speakerPin = 8;
const int potPin = 2;
const int phoPin = 0;

const float vibAmount = 75;
const float vibFreq = 2.8;
const float note0 = NOTE_C6;
const float note1 = NOTE_C5;
const float note2 = NOTE_D5;
const float note3 = NOTE_E5;
const float note4 = NOTE_F5;
const float note5 = NOTE_G5;
const float note6 = NOTE_A5;
const float note7 = NOTE_B5;


int btn1 = 0; // variable for reading the pushbutton status
int btn2 = 0;
int btn3 = 0;
int btn4 = 0;
float t = 0; // time
float pot = 0; // potentionmeter value
float pho = 0; // photoresistor

void setup() {
  pinMode(btn1Pin, INPUT);
  pinMode(btn2Pin, INPUT);
  pinMode(btn3Pin, INPUT);
  pinMode(btn4Pin, INPUT);
  pinMode(speakerPin, OUTPUT);

  // Serial.begin(9600); // uncomment to enable debugging
}

void loop() {
  t += 1;
  if(t > 32000) {
    t = -32000;
  }

  // Read pin states
  pot = analogRead(potPin);
  pho = analogRead(phoPin);
  btn1 = digitalRead(btn1Pin);
  btn2 = digitalRead(btn2Pin);
  btn3 = digitalRead(btn3Pin);
  btn4 = digitalRead(btn4Pin);

  float mod = map(pho, 200, 600, -300, 300);
  float baseTone = sin(t / vibFreq) * vibAmount * (pot / 1024);
  float note = -1;

  if     (btn1 == HIGH && btn2 == HIGH) { note = note2; }
  else if(btn2 == HIGH && btn3 == HIGH) { note = note4; }
  else if(btn3 == HIGH && btn4 == HIGH) { note = note6; }
  else if(btn4 == HIGH && btn1 == HIGH) { note = note0; }
  else if(btn1 == HIGH) { note = note1; }
  else if(btn2 == HIGH) { note = note3; }
  else if(btn3 == HIGH) { note = note5; }
  else if(btn4 == HIGH) { note = note7; }

  if(note != -1) {
    tone(speakerPin, baseTone + note + mod);
  } else {
    noTone(speakerPin);
  }
}
